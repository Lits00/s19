let username,password,role;
function login(){
    let username = prompt("Enter your username:");
    let password = prompt("Enter your password:");
    let role = prompt("Enter your role:").toLowerCase();
    if (username === null || username === '' && password === null || password === '' && role === null || role === ''){
        alert("Input must not be empty!");
    } 
    else {
        switch(role){
            case 'admin':
                alert("Welcome back to the class portal, admin!");
                break;
            case 'teacher':
                alert("Welcome back to the class portal, teacher!");
                break;
            case 'student':
                alert("Welcome back to the class portal, student!");
                break;
            default:
                alert("Role out of range.");
                break;
        }
    }
}
login();

function getAverage(x, y, z, a){
    let average = Math.round((x+y+z+a)/4)
    if (role === "teacher" || role === "admin" || role === undefined || role === null){
        alert(role + "! You are not allowed to acces this feature!")
    } 
    else {
        
        if(average <= 74){
            console.log(`Hello, student, your average is ${average}. The letter equivalent is F`);
        } 
        else if (average >= 75 && average <= 79) {
            console.log(`Hello, student, your average is ${average}. The letter equivalent is D`);
        }
        else if (average >= 80 && average <= 84) {
            console.log(`Hello, student, your average is ${average}. The letter equivalent is C`);
        }
        else if (average >= 85 && average <= 89) {
            console.log(`Hello, student, your average is ${average}. The letter equivalent is B`);
        }
        else if (average >= 90 && average <= 95) {
            console.log(`Hello, student, your average is ${average}. The letter equivalent is A`);
        }
        else if (average >= 96) {
            console.log(`Hello, student, your average is ${average}. The letter equivalent is A+`);
        }
    }
}

